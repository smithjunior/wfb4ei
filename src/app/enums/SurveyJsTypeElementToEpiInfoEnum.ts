export enum SurveyJsTypeElementToEpiInfoEnum {

  Text = 1,
  Text_Upper = 3,
  Comment = 4,
  Number = 5,
  Date = 7,
  Checkbox = 10,
  Select = 17,
  Panel = 21,
  Radio = 12

}
