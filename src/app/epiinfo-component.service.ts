import { Injectable } from '@angular/core';
import * as FasterXmlParser from 'fast-xml-parser';
import { TemplateEpiInfoController } from './controllers/TemplateEpiInfoController';

@Injectable({
  providedIn: 'root'
})
export class EpiinfoComponentService {

  constructor() { }

  transformJsonToXml(jsonSurveyJs: any): any {

    const templateEpiInfoController = new TemplateEpiInfoController(jsonSurveyJs);
    const ParserLocal = FasterXmlParser.j2xParser;
    const parser = new ParserLocal(this.returnDefaultOptions());
    return parser.parse(JSON.parse(templateEpiInfoController.getEpiInfoXmlString()));
  }

  private returnDefaultOptions(): any {
    return {
      attributeNamePrefix : '',
      attrNodeName: '@', // default is false
      textNodeName : '#text',
      ignoreAttributes : true,
      cdataTagName: '__cdata', // default is false
      cdataPositionChar: '\\c',
      format: true,
      indentBy: '  ',
      supressEmptyNode: true
    };
  }
}
