export class TemplateEpiInfoComponent {
  name: string;
  description: string;
  createDate: string;
  level: string;

  constructor() {}

  getProperties(): string {
    return `{ "Name": "${this.name}",
              "Description": "${this.description}",
              "CreateDate" :"${this.createDate}",
              "Level": "${this.level}"
              }`;
  }

}
