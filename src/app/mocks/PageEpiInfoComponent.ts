import {ViewEpiInfoComponent} from './ViewEpiInfoComponent';

export class PageEpiInfoComponent {

  pageId: any;
  name: any;
  position: any ;
  private _backgroundId: any = `0`;
  view: ViewEpiInfoComponent = null;

  elements: string[];

  constructor() {  }

  private getFields(): string {
    if ( this.elements.length <= 0 ) {
      return ``;
    }
    return `, "Field":[${this.elements.join(`,`)}]`;
  }

  getProperties(): string {
    return `{ "@":{ "PageId":"${this.pageId}",
                    "Name":"${this.name}",
                    "Position":"${this.position}",
                    "BackgroundId":"${this._backgroundId}",
                    "ViewId":"${this.view.viewId}"
                  }
                  ${this.getFields()}
              }`;
  }
}
