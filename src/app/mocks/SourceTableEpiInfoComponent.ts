export class SourceTableEpiInfoComponent {

  tableName: any;
  items: string[];

  constructor() {
  }

  private getItens(): string {
    if (this.items.length <= 0 ) {
      return ``;
    }
    return `, "Item": [ ${this.items.join(`,`)} ]`;
  }
  getProperties(): string {
    return ` { "@" :{ "TableName": "${this.tableName}" }
            ${this.getItens()}
            }`;
  }
}
