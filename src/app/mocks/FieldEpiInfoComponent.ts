import { Guid } from 'guid-typescript';

export class FieldEpiInfoComponent {

  fieldId: any; // Dynamic id
  promptText: any;
  name: any;
  pageId: any;
  pageName: any;
  fieldTypeId: any = `1`;
  isRequired: any = `False`;
  isReadOnly: any = `False`;
  maxLength: any = `0`;
  list: any = ``;
  sourceTableName: any = ``;
  textColumnName: any = ``;
  codeColumnName: any = ``;
  promptTopPositionPercentage: any = ``; // 0,019685039 - _controlTopPositionPercentage
  controlTopPositionPercentage: any = ``; // 0,0177165354330709 first element
  promptLeftPositionPercentage: any = `0,00641025641025641`; // Fixed based on tests
  pattern: any = ``;
  showTextOnRight: any = `False`;
  sort: any = `False`;
  /**
   * if  newElement.promptTopPositionPercentage + _controlHeightPercentage  > 1.0 new Page,
   * next element promptTopPositionPercentage = previous element.promptTopPositionPercentage +  _controlHeightPercentage
   */
  controlHeightPercentage: any = `0,0246062992125984`; // Fixed based on tests
  private _controlLeftPositionPercentage: any = `0,00256410256410256`; // Fixed based on tests
  private _controlWidthPercentage: any = `0,992307692307692`; // Fixed based on tests
  private _uniqueId: Guid;
  private _controlAfterCheckCode: any = ``;
  private _controlBeforeCheckCode: any = ``;
  private _pageBeforeCheckCode: any = ``;
  private _pageAfterCheckCode: any = ``;
  private _position: any = `0`;
  private _expr1015: any = `Segoe UI`;
  private _expr1016: any = `10`;
  private _expr1017: any = `Regular`;
  private _controlScriptName: any = ``;
  private _promptFontFamily: any = `Segoe UI`;
  private _promptFontSize: any = `10`;
  private _promptFontStyle: any = `Regular`;
  private _controlFontFamily: any = `Arial`;
  private _controlFontSize: any = `10`;
  private _controlFontStyle: any = `Regular`;
  private _promptScriptName: any = ``;
  private _shouldRepeatLast: any = `False`;
  private _shouldRetainImageSize: any = `False`;
  private _lower: any = ``;
  private _upper: any = ``;
  private _relateCondition: any = ``;
  private _shouldReturnToParent: any = `False`;
  private _relatedViewId: any = ``;

  private _isExclusiveTable: any = `False`;
  private _tabIndex: any = `0`;
  private _hasTabStop: any = `True`;
  private _sourceFieldId: any = ``;

  constructor( ) {
    this._uniqueId = Guid.create();
  }

  getProperties(): string {
    return `{ "@":{
              "Name":"${this.name}",
              "PageId":"${this.pageId}",
              "FieldId":"${this.fieldId}",
              "UniqueId":"${this._uniqueId}",
              "FieldTypeId":"${this.fieldTypeId}",
              "ControlAfterCheckCode":"${this._controlAfterCheckCode}",
              "ControlBeforeCheckCode":"${this._controlBeforeCheckCode}",
              "PageName":"${this.pageName}",
              "PageBeforeCheckCode":"${this._pageBeforeCheckCode}",
              "PageAfterCheckCode":"${this._pageAfterCheckCode}",
              "Position":"${this._position}",
              "ControlTopPositionPercentage":"${this.controlTopPositionPercentage}",
              "ControlLeftPositionPercentage":"${this._controlLeftPositionPercentage}",
              "ControlHeightPercentage":"${this.controlHeightPercentage}",
              "ControlWidthPercentage":"${this._controlWidthPercentage}",
              "Expr1015":"${this._expr1015}",
              "Expr1016":"${this._expr1016}",
              "Expr1017":"${this._expr1017}",
              "ControlScriptName":"${this._controlScriptName}",
              "PromptTopPositionPercentage":"${this.promptTopPositionPercentage}",
              "PromptLeftPositionPercentage":"${this.promptLeftPositionPercentage}",
              "PromptText":"${this.promptText}",
              "PromptFontFamily":"${this._promptFontFamily}",
              "PromptFontSize":"${this._promptFontSize}",
              "PromptFontStyle":"${this._promptFontStyle}",
              "ControlFontFamily":"${this._controlFontFamily}",
              "ControlFontSize":"${this._controlFontSize}",
              "ControlFontStyle":"${this._controlFontStyle}",
              "PromptScriptName":"${this._promptScriptName}",
              "ShouldRepeatLast":"${this._shouldRepeatLast}",
              "IsRequired":"${this.isRequired}",
              "IsReadOnly":"${this.isReadOnly}",
              "ShouldRetainImageSize":"${this._shouldRetainImageSize}",
              "Pattern":"${this.pattern}",
              "MaxLength":"${this.maxLength}",
              "ShowTextOnRight":"${this.showTextOnRight}",
              "Lower":"${this._lower}",
              "Upper":"${this._upper}",
              "RelateCondition":"${this._relateCondition}",
              "ShouldReturnToParent":"${this._shouldReturnToParent}",
              "RelatedViewId":"${this._relatedViewId}",
              "List":"${this.list}",
              "SourceTableName":"${this.sourceTableName}",
              "CodeColumnName":"${this.codeColumnName}",
              "TextColumnName":"${this.textColumnName}",
              "Sort":"${this.sort}",
              "IsExclusiveTable":"${this._isExclusiveTable}",
              "TabIndex":"${this.fieldId}",
              "HasTabStop":"${this._hasTabStop}",
              "SourceFieldId":"${this._sourceFieldId}"
                }
              }` ;
  }

}
