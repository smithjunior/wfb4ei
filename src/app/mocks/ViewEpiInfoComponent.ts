export class ViewEpiInfoComponent {

  viewId: any;
  name: any;
  checkCode: any = ``;
  pages: string[];
  private _isRelatedView: any = `False`;
  private _width: any = `780`;
  private _height: any =  `1016`;
  private _orientation: any = `Portrait`;
  private _labelAlign: any = `Vertical`;

  constructor() {
  }

  private getPages(): string {
    if (this.pages.length <= 0) {
      return ``;
    }
    return `, "Page":[ ${this.pages.join(',')} ]`;
  }

  getProperties(): string {
    return `{ "@":{ "ViewId":"${this.viewId}",
      "Name":"${this.name}",
      "IsRelatedView":"${this._isRelatedView}",
      "CheckCode":"${this.checkCode}",
      "Width":"${this._width}",
      "Height":"${this._height}",
      "Orientation":"${this._orientation}",
      "LabelAlign":"${this._labelAlign}" }
      ${this.getPages()}
      }`;
  }
}
