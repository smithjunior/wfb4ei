import { TestBed, inject } from '@angular/core/testing';

import { EpiinfoComponentService } from './epiinfo-component.service';

describe('EpiinfoComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EpiinfoComponentService]
    });
  });

  it('should be created', inject([EpiinfoComponentService], (service: EpiinfoComponentService) => {
    expect(service).toBeTruthy();
  }));
});
