import {Component, Input} from '@angular/core';
import * as Survey from 'survey-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'WFB4EI';

  json = {};

  onSurveySaved(survey) {
    this.json = survey;
  }

}
