export class SourceTableEpiInfoController {

  private _choicesJson: any;
  private _sourceTableName: any;

  constructor(choicesJson: any, sourceTableName: any) {
    this._choicesJson = choicesJson;
    this._sourceTableName = sourceTableName;
  }

  createSourceTableOnLocalStorage(): boolean {
    const choicesArray: string[] = this._choicesJson.map(item => typeof item === `object` ? item.text : item);
    localStorage.setItem(`${this._sourceTableName}`, choicesArray.join(`|`));
    return true;
  }
}
