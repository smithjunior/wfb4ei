import { FieldEpiInfoComponent } from '../mocks/FieldEpiInfoComponent';
import { SurveyJsTypeElementToEpiInfoEnum } from '../enums/SurveyJsTypeElementToEpiInfoEnum';
import {SourceTableEpiInfoController} from './SourceTableEpiInfoController';

export class FieldEpiInfoController {

  private _fieldEpiInfoComponent: FieldEpiInfoComponent;
  private _fieldJson: any;
  private _defaultTopPercent: any = `0,0177165354330709`;
  private _startTopPercent: any;
  private _elementNumber: any;
  private _lastControlTopPercent: any;
  private _lastHeightPercent = ``;

  constructor( elementJson: any, pageMock: any, elementNumber: any, lastControlTopPercent: any, lastHeightPercent: any ) {
    this._startTopPercent = this._defaultTopPercent;
    this._elementNumber = elementNumber;
    this._lastControlTopPercent = lastControlTopPercent;
    this._fieldJson = elementJson;
    this._lastHeightPercent = lastHeightPercent;

    this._fieldEpiInfoComponent = new FieldEpiInfoComponent();
    this._fieldEpiInfoComponent.name = `component${elementNumber}`;
    this._fieldEpiInfoComponent.pageName = pageMock.name;
    this._fieldEpiInfoComponent.pageId = pageMock.pageId;
    this._fieldEpiInfoComponent.promptText = 'title' in elementJson ? elementJson.title : elementJson.name;
    this._fieldEpiInfoComponent.fieldId = elementNumber;
    this._fieldEpiInfoComponent.controlTopPositionPercentage = this._startTopPercent;
    this.calculateHeight();
    this._fieldEpiInfoComponent.promptTopPositionPercentage = this.getPromptTopPositionPercentage();
    this._fieldEpiInfoComponent.fieldTypeId = this.getTypeElementEpiInfo(elementJson);

  }

  private calculateHeight(): void {
    if (this._lastControlTopPercent !== ``) {
      this._fieldEpiInfoComponent.controlTopPositionPercentage = ((parseFloat(this._lastControlTopPercent.replace(`,`, `.`)) +
        parseFloat(this.getHeightPercentage())
          + 0.009476378) + ``)
        .replace(`.`, `,`);
    }
  }

  private getHeightPercentage(): string {
    if (this._lastHeightPercent === ``) {
      return this._fieldEpiInfoComponent.controlHeightPercentage.replace(`,`, `.`);
    }
    return this._lastHeightPercent.replace(`,`, `.`);
  }

  getFieldEpiInfoComponent(): FieldEpiInfoComponent {
    return this._fieldEpiInfoComponent;
  }
  private getRadioHeight(): string {
    return (parseFloat(this.getHeightPercentage()) + 0.050476378 + ``)
  .replace(`.`, `,`);
  }

  private getPromptTopPositionPercentage() {
    if (this._lastControlTopPercent !== ``) {
      return ((parseFloat(this._lastControlTopPercent.replace(`,`, `.`)) +
        parseFloat(this.getHeightPercentage())
        - 0.01476378) + ``)
        .replace(`.`, `,`);
    }
    return ((parseFloat(this._startTopPercent.replace(`,`, `.`)) - 0.01476378) + ``)
                                              .replace(`.`, `,`);
  }

  private getTypeElementEpiInfo(elementJson: any): any {
    switch (elementJson.type) {
      case `text`: {
        return SurveyJsTypeElementToEpiInfoEnum.Text;
      }
      case `boolean`: {
        this._fieldEpiInfoComponent.promptText = `label` in this._fieldJson ? this._fieldJson.label : this._fieldJson.name;
        return SurveyJsTypeElementToEpiInfoEnum.Checkbox;
      }
      case `radiogroup`: {
        this._fieldEpiInfoComponent.list = this.getChoicesString();
        this._fieldEpiInfoComponent.controlHeightPercentage = this.getRadioHeight();
        this._fieldEpiInfoComponent.pattern = `Vertical,Left`;
        this._fieldEpiInfoComponent.showTextOnRight = `True`;
        return SurveyJsTypeElementToEpiInfoEnum.Radio;
      }
      case `dropdown`: {
        this._fieldEpiInfoComponent.textColumnName = this._fieldEpiInfoComponent.codeColumnName = this._fieldEpiInfoComponent.name;
        this._fieldEpiInfoComponent.sort = `True`;
        this._fieldEpiInfoComponent.sourceTableName = `db_sourceTable_${this._fieldEpiInfoComponent.name}`;
        const sourceTableEpiInfoController = new SourceTableEpiInfoController(
                                                                              this._fieldJson.choices,
                                                                              this._fieldEpiInfoComponent.sourceTableName
                                                                            );
        sourceTableEpiInfoController.createSourceTableOnLocalStorage();
        return SurveyJsTypeElementToEpiInfoEnum.Select;
      }
      default: {
        return SurveyJsTypeElementToEpiInfoEnum.Text;
      }
    }
  }

  private getChoicesString(): string {
      const choicesStringArray: string[] = this._fieldJson.choices.map(item => typeof item === `object` ? item.text : item);
      return choicesStringArray.join(`,`);
  }

}
