import { TemplateEpiInfoComponent } from '../mocks/TemplateEpiInfoComponent';
import { PageEpiInfoComponent } from '../mocks/PageEpiInfoComponent';
import { ViewEpiInfoComponent } from '../mocks/ViewEpiInfoComponent';
import * as moment from 'moment';
import { FieldEpiInfoController } from './FieldEpiInfoController';
import {SourceTableEpiInfoComponent} from '../mocks/SourceTableEpiInfoComponent';

export class TemplateEpiInfoController {

  private _pageNumber = 1;
  private _elementNumber = 1;
  private _surveyJson: any;
  private _lastTopPercent: any = ``;
  private _lastHeightPercent: any = ``;
  private _viewMock: ViewEpiInfoComponent;

  constructor(surveyJson: any) {
    this._surveyJson = surveyJson;


    this._viewMock  = new ViewEpiInfoComponent();
    this._viewMock.viewId = `1`;
    this._viewMock.name = `form01`;
  }

  getEpiInfoXmlString(): string {

    const stringMock = `{ "Template":  { "@": ${this.getTemplateMock().getProperties()}, "Project" :
    { "View": [ ${this.getPagesElementsMocky()} ] } ${this.getSourceTableMocky()} } }`;
    localStorage.clear();
    return stringMock;
  }

  private getSourceTableMocky(): string {
    const sourceTables = this.getSourceTablesStorage();

    if (sourceTables === ``) {
      return ``;
    }
    return `, "SourceTable": [${sourceTables}] `;
  }

  private getSourceTablesStorage(): string {
    const self = this;
    if (localStorage.length <= 0) {
      return ``;
    }
    const sourceTableStringArray: string[] = [];

    Object.keys(localStorage).forEach(function (keyStorage) {

      const sourceTablePatternNameRe = new RegExp(`db_sourceTable_{0,14}`, `g`);

      if (sourceTablePatternNameRe.exec(keyStorage)) {
        const sourceTableMock = new SourceTableEpiInfoComponent();
        sourceTableMock.tableName = `${keyStorage}` ;
        sourceTableMock.items = self.getItensMock(localStorage.getItem(keyStorage), keyStorage.substring(15, keyStorage.length ));
        sourceTableStringArray.push(sourceTableMock.getProperties());
      }
    });
    return sourceTableStringArray.join(`,`);
  }

  private getItensMock(itemsStorageString: string, sourceTableName: string): any {
   const itemsStorageArray =  itemsStorageString.split(`|`);
   return itemsStorageArray.map(item => `{ "@": { "${sourceTableName}": "${item}"} } `);
  }

  private getTemplateMock(): TemplateEpiInfoComponent {

    const templateMock = new TemplateEpiInfoComponent();

    templateMock.name = `EpiInfoProject${moment().format(`YYYYmD`)}`;
    templateMock.createDate = moment().format(`dddd, LLL`);
    templateMock.description = `Project EpiInfo Created on Web Form Builder ${moment().format(`YYYY`)}`;
    templateMock.level = `View`;

    return templateMock;
  }

  private getPagesElementsMocky(): any {
    this._viewMock.pages = this._surveyJson.pages.map( item => this.getPageMock(item) );
    return this._viewMock.getProperties();
  }

  private getPageMock( pageJson: any): any {
    const pageMock = new PageEpiInfoComponent();
    pageMock.name = 'title' in pageJson  ? pageJson.title : `page${this._pageNumber}`;
    pageMock.pageId = this._pageNumber;
    pageMock.position = 0;
    pageMock.view = this._viewMock;
    pageMock.elements = pageJson.elements.map(item => this.getElementEpiInfoField(item, pageMock));
    this._pageNumber++;
    this._elementNumber = 1;
    return pageMock.getProperties();
  }

  private getElementEpiInfoField( elementJson: any, pageMock: PageEpiInfoComponent ): string {
    const fieldEpiInfoController = new FieldEpiInfoController(elementJson,
                                                              pageMock,
                                                              this._elementNumber,
                                                              this._lastTopPercent,
                                                              this._lastHeightPercent);
    this._elementNumber++;
    this._lastTopPercent = fieldEpiInfoController.getFieldEpiInfoComponent().controlTopPositionPercentage;
    this._lastHeightPercent = fieldEpiInfoController.getFieldEpiInfoComponent().controlHeightPercentage;
    return fieldEpiInfoController.getFieldEpiInfoComponent().getProperties();
  }

}
