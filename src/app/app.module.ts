import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { SurveyComponent } from './survey.component';
import { SurveyEditorComponent } from './survey.editor.component';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    SurveyComponent,
    SurveyEditorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
