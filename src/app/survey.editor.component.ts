import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import * as SurveyKo from 'survey-knockout';
import * as SurveyEditor from 'surveyjs-editor';
import * as widgets from 'surveyjs-widgets';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { EpiinfoComponentService } from './epiinfo-component.service';
import * as xml2js from 'xml2js';
import * as he from 'he';
import * as FasterXmlParser from 'fast-xml-parser';
import 'inputmask/dist/inputmask/phone-codes/phone.js';
import {j2xParser} from 'fast-xml-parser';

widgets.select2(SurveyKo);

@Component({
  selector: 'app-survey-editor',
  template: `<div id="surveyEditorContainer"></div>`
})
export class SurveyEditorComponent implements OnInit {

  editor: SurveyEditor.SurveyEditor;

  @Input() json: any;

  @Output() surveySaved: EventEmitter<Object> = new EventEmitter();

  constructor(private ngxXml2jsonService: NgxXml2jsonService, private epiinfoComponentService: EpiinfoComponentService) {}

  ngOnInit(): void {

    const self = this;
    /**
    * Customm Properties
    * SurveyKo.JsonObject.metaData.addProperty(
    *  "questionbase",
    *  "popupdescription:text"
    * );
    * SurveyKo.JsonObject.metaData.addProperty("page", "popupdescription:text");
    */
    SurveyEditor.editorLocalization.currentLocale = `pt`;

    SurveyEditor.editorLocalization.locales[`pt`].ed[`testSurvey`] = `Testar Formulário`;
    SurveyEditor.editorLocalization.locales[`pt`].ed[`designer`] = `Designer de Formulário`;
    SurveyEditor.editorLocalization.locales[`pt`].ed[`saveSurvey`] = `Salvar Formulário`;
    SurveyEditor.editorLocalization.locales[`pt`].qt[`boolean`] = `Checkbox único`;

    SurveyEditor.StylesManager.applyTheme('darkblue');

    const editorOptions = {
        generateValidJSON: true,
        showPropertyGrid: false,
        questionTypes: [
                      'text',
                      'dropdown',
                      'radiogroup',
                      'boolean',
                      'select2'
                    ]
      };

    self.editor = new SurveyEditor.SurveyEditor(
      'surveyEditorContainer',
      editorOptions
    );

    self.editor.saveSurveyFunc = self.saveMySurvey;
    const surveySettingsAction = self.editor.toolbarItems().filter(function(item) {
      return item.id === `svd-survey-settings`;
    })[0];
    self.editor.toolbarItems.remove(surveySettingsAction);
    self.editor.toolbarItems
      .push({
        id: 'importTemplateEpiInfo',
        visible: true,
        title: 'Importar Template Epi Info',
        enabled: true,
        action: function () {

          const element: HTMLElement = document.getElementById('file-input');
          element.click();
          element.onchange = function (e: any) {
            const files = e.target.files || e.dataTransfer.files;
            if (!files.length) {
              return;
            }
            const reader = new FileReader();
            reader.onload = (event: any): void => {
              const arquivo_xml = new DOMParser().parseFromString(event.target.result, 'text/xml');
              self.convertXmlToJsonFasterXmlParser(arquivo_xml);
            };
            reader.readAsText(files[0]);
          };
          return false;
        }
      });

    self.editor.toolbarItems
      .push({
        id: 'exportTemplateEpiInfo',
        visible: true,
        title: 'Exportar Template Epi Info',
        enabled: true,
        action: function () {
          const xml = self.epiinfoComponentService.transformJsonToXml(JSON.parse(self.editor.text));
          if ( xml ) {
            const filename = 'TemplateEpiInfo.xml';
            const xmlElement = document.createElement('a');
            const bb = new Blob([xml], {type: 'text/plain'});

            xmlElement.setAttribute('href', window.URL.createObjectURL(bb));
            xmlElement.setAttribute('download', filename);
            xmlElement.setAttribute(`id`, `IdDownloadTemplate`);
            xmlElement.dataset.downloadurl = ['text/plain', xmlElement.download, xmlElement.href].join(':');
            xmlElement.draggable = true;
            xmlElement.classList.add('dragout');
            xmlElement.click();
          }
          return true;
        }
      });
    document.querySelector(`.svd_commercial_container`).remove();
  }

  /**
   * Save Function
   * */
  saveMySurvey = () => {
    console.log(JSON.parse(this.editor.text));
    this.surveySaved.emit(JSON.parse(this.editor.text));
  }

  /**
   * Convert Xml To Json function
   * with NgxXml2JsonService
   * @param xml any
   * */
  convertXmlToJsonNgxXml2Json = (xml: any) => {
    return this.ngxXml2jsonService.xmlToJson(xml);
  }

  /**
   * Convert Xml To Json function
   * with xml2js componet
   * @param xml any
   * */
  convertXmlToJsonXml2Js = (xml: any) => {
    let result;
    const xmlText = new XMLSerializer().serializeToString(xml);
    xml2js.Parser().parseString(xmlText, (e, r) => result = r);
  }

  /**
   * Convert Xml To Json function
   * with FasterXmlParser componet
   * @param xml any
   * */
  convertXmlToJsonFasterXmlParser = (xml: any) => {
    const xmlText = new XMLSerializer().serializeToString(xml);
    const options = {
      // attrNodeName: 'false',//default is 'false'
      // textNodeName : "#text",
      attributeNamePrefix : '',
      ignoreAttributes : false,
      ignoreNameSpace : false,
      allowBooleanAttributes : false,
      parseNodeValue : true,
      parseAttributeValue : false,
      trimValues: true,
      cdataTagName: '__cdata', // default is 'false'
      cdataPositionChar: '\\c',
      localeRange: '', // To support non english character in tag/attribute values.
      parseTrueNumberOnly: false,
      attrValueProcessor: a => he.decode(a, {isAttributeValue: true}), // default is a=>a
      tagValueProcessor : a => he.decode(a) // default is a=>a
    };

    if (true === FasterXmlParser.validate(xmlText)) {
      const objXmlJson: any = FasterXmlParser.parse(xmlText, options);
      const surveyJson = this.createJsonSurveyJS(objXmlJson);
      this.editor.text = JSON.stringify(surveyJson);
      return true;
    }
    alert('Invalid XML');
    return false;
  }

  /**
   * Created the Survey Json Structure
   * */
  createJsonSurveyJS = (jsonXml: any) => {
    if ( jsonXml.Template.hasOwnProperty(`SourceTable`) ) {
      this.loadSourceTables(jsonXml.Template.SourceTable);
    }
    return {
      title: jsonXml.Template.Name,
      pages: this.getPages(jsonXml.Template)
    };
  }

  /**
   * Get Value By key from objet
   * */
   getMapValue = (obj: any, key: any): any => {
    if (obj.hasOwnProperty(key)) {
      return obj[key];
    }
    throw new Error(` Invalid map key.${key} ` );
  }

  /**
   * Get all pages from jsonTemplate
   * */
  getPages = (jsonTemplate: any): any => {
    const pages = [];
    if (jsonTemplate.Project.View  instanceof Object) {
      Object.entries(jsonTemplate.Project.View.Page).forEach(
        ([key, value]) => !Number.isNaN(Number.parseInt(key)) ? pages.push({
          name: 'page' + key,
          title: this.getMapValue(value, 'Name'),
          elements: this.getElements(this.getMapValue(value, 'Field'))
        }) : null
      );
      if ( 0 < pages.length) {
        return pages;
      }
      return [{
        name: 'page0',
        title: jsonTemplate.Project.View.Page.Name,
        elements: this.getElements(jsonTemplate.Project.View.Page.Field)
      }];
    }
  }

  /**
   * Get All Elements From jsonPage
   * */
  getElements = (jsonPage: any): any => {
    const elements = [];
    for (const element of jsonPage) {
      const elementJson = {
        name: element.Name,
        title: element.PromptText
      };
      Object.assign(elementJson, this.getTypeElement(element.FieldTypeId));
      if ('' !== element.SourceTableName ) {
        Object.assign(elementJson, {'choices': this.getSourceTable(element.SourceTableName)});
      }
      if (element.List !== ``) {
        Object.assign(elementJson, {'choices': element.List.split(`||`)[0].split(`,`)});
      }
      elements.push(elementJson);
    }
    return elements;
  }

  /**
   * Load Source Tables In Memory
   * */
  loadSourceTables = (jsonTableSources: any): any => {
    if (`TableName` in jsonTableSources) {
      localStorage.setItem(jsonTableSources.TableName, this.getChoicesFromItems(jsonTableSources.Item));
      return true;
    }

    Object.entries(jsonTableSources).forEach(
      ([key, value]) => {
        localStorage.setItem(this.getMapValue(value, 'TableName'), this.getChoicesFromItems(this.getMapValue(value, 'Item')));
      }
      );
    return true;
  }

  /**
   *  Get Choices from Items Json
   * */
  getChoicesFromItems = (jsonItems: any): string => {
    const choices: any = new Array();
    Object.entries(jsonItems).forEach(
      ([key, value]) => { choices.push(this.getChoiceString(value)); }
      );
    return choices;
  }

  /**
   * Get String from Item
   * */
  getChoiceString = (jsonItem): string => {
    let choice = '';
    for (const [key, value] of Object.entries(jsonItem)) {
      choice = value.toString();
    }
    return choice;
  }

  /**
   * Get Specific Source Table from localStorage inside browser
   * */
  getSourceTable = (tableSourceName: string): any => {
    return localStorage.getItem(tableSourceName).split(',');
  }

  /**
   * get element type from jsonField
   * */
  getTypeElement = (elementType: any) => {

    const typeElements = {
      'CAMPO_LABEL_TITLE': '2',
      '1': {type: 'text'}, // text
      '3': {type: 'text'}, // text_uppercase
      '4': {type: 'comment'}, // multiline
      'CAMPO_UNIQUEIDENTIFIER': '25',
      '5': {type: 'text'}, // number
      'CAMPO_PHONENUMBER': '6',
      '7': {type: 'text', inputType: 'date'}, // date
      'CAMPO_TIME': '8',
      'CAMPO_DATE_TIME': '9',
      '10': {type: 'boolean', defaultValue: 'false', custom: 'false'}, // checkbox
      'CAMPO_YES_NO': '11',
      'CAMPO_OPTION': '12',
      'CAMPO_COMMANDBUTTON': '13',
      'CAMPO_IMAGE': '14',
      'CAMPO_MIRROR': '15',
      'CAMPO_GRID': '16',
      '12': {type: `radiogroup`},
      '17': {type: 'dropdown', colCount: 0}, // select
      'CAMPO_COMMENT_LEGAL': '19',
      'CAMPO_CODES': '18',
      'CAMPO_RELATE': '20',
      '21': {type: 'panel'}, // group
    };
    if ( typeof  typeElements[elementType] === 'undefined') {
      return '{}';
    }
    return typeElements[elementType];
  }
}
